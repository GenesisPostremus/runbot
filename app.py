import os, sys
import re
import time
import atexit
from typing import Dict, List
import logging
import asyncio
import requests
import urllib.parse, urllib.request
import nextcord
from nextcord.ext import commands
from nextcord.ui import View, Button
import yt_dlp

sys.path.append('.')
logging.basicConfig(level=logging.WARNING)
yt_dlp.utils.bug_reports_message = lambda: ''  # disable yt_dlp bug report
intents = nextcord.Intents.default()
# noinspection PyDunderSlots
intents.message_content = True
# help_command = commands.DefaultHelpCommand(no_category='Commands')  # Change only the no_category default string
help_command=None
bot = commands.Bot(command_prefix=commands.when_mentioned_or('+'), intents=intents, help_command=help_command, strip_after_prefix=True, description='Liste des commandes')

ytdl_format_options = {'format': 'bestaudio',
                       'outtmpl': '%(extractor)s-%(id)s-%(title)s.%(ext)s',
                       'restrictfilenames': True,
                       'no-playlist': True,
                       'nocheckcertificate': True,
                       'ignoreerrors': False,
                       'logtostderr': False,
                       'geo-bypass': True,
                       'quiet': True,
                       'no_warnings': True,
                       'default_search': 'auto',
                       'source_address': '0.0.0.0'}
ffmpeg_options = {'options': '-vn -sn'}
ytdl = yt_dlp.YoutubeDL(ytdl_format_options)

class Source:
    """Parent class of all music sources"""

    def __init__(self, audio_source: nextcord.AudioSource, metadata):
        self.audio_source: nextcord.AudioSource = audio_source
        self.metadata = metadata
        self.title: str = metadata.get('title', 'Unknown title')
        self.url: str = metadata.get('url', 'Unknown URL')

    def __str__(self):
        return f'{self.title} ({self.url})'

class YTDLSource(Source):
    """Subclass of YouTube sources"""

    def __init__(self, audio_source: nextcord.AudioSource, metadata):
        super().__init__(audio_source, metadata)
        self.url: str = metadata.get('webpage_url', 'Unknown URL')  # yt-dlp specific key name for original URL

    @classmethod
    async def from_url(cls, url, *, loop=None, stream=True):
        loop = loop or asyncio.get_event_loop()
        metadata = await loop.run_in_executor(None, lambda: ytdl.extract_info(url, download=not stream))
        if 'entries' in metadata: metadata = metadata['entries'][0]
        filename = metadata['url'] if stream else ytdl.prepare_filename(metadata)
        return cls(await nextcord.FFmpegOpusAudio.from_probe(filename, **ffmpeg_options), metadata)

class ServerSession:
    def __init__(self, guild_id, voice_client):
        self.guild_id: int = guild_id
        self.voice_client: nextcord.VoiceClient = voice_client
        self.queue: List[Source] = []

    # Gestion de la queue
    def display_queue(self) -> str:
        currently_playing = f'Currently playing: 0. {self.queue[0].title}'
        return currently_playing + '\n' + '\n'.join([f'{i + 1}. {s}' for i, s in enumerate(self.queue[1:])])

    async def add_to_queue(self, ctx, url):  # does not auto start playing the playlist
        yt_source = await YTDLSource.from_url(url, loop=bot.loop, stream=False)  # stream=True has issues and cannot use Opus probing
        self.queue.append(yt_source)
        if self.voice_client.is_playing():
            async with ctx.typing():
                embed = nextcord.Embed(title="C\'est ajouté bg", description=f"{yt_source.title}", color=0x552095)
                embed.set_footer(text="RunBot V3.1")
                skip_interact = Button(label="Skip", custom_id="skip", style=nextcord.ButtonStyle.primary)

                async def skip_song(ctx):
                    """Skip the current song"""
                    guild_id = ctx.guild.id
                    if guild_id in server_sessions:
                        session = server_sessions[guild_id]
                        voice_client = session.voice_client
                        if voice_client.is_playing():
                            if len(session.queue) > 1:
                                voice_client.stop()
                            else:
                                embed = nextcord.Embed(title="Finito", description="YAR après !", color=0x552095)
                                embed.set_footer(text="RunBot V3.1")
                                await ctx.send(embed=embed)
                
                skip_interact.callback = skip_song

                myview = View(timeout=180)
                myview.add_item(skip_interact)
                await ctx.send(embed=embed, view=myview)
            pass  # to stop the typing indicator

    # Lecture de la queue
    async def start_playing(self, ctx):
        async with ctx.typing():
            self.voice_client.play(self.queue[0].audio_source, after=lambda e=None: self.after_playing(ctx, e))
        await base_message(ctx, self.queue[0].title)

    async def after_playing(self, ctx, error):
        if error:
            raise error
        else:
            if self.queue:
                await self.play_next(ctx)

    async def play_next(self, ctx):  # should be called only after making the first element of the queue the song to play
        self.queue.pop(0)
        if self.queue:
            async with ctx.typing():
                await self.voice_client.play(self.queue[0].audio_source, after=lambda e=None: self.after_playing(ctx, e))
            await base_message(ctx, self.queue[0].title)

server_sessions: Dict[int, ServerSession] = {}  # {guild_id: ServerSession}

def clean_cache_files():
    if not server_sessions:  # only clean if no servers are connected
        for file in os.listdir():
            if os.path.splitext(file)[1] in ['.webm', '.mp4', '.m4a', '.mp3', '.ogg'] and time.time() - os.path.getmtime(file) > 7200:  # remove all cached webm files older than 2 hours
                os.remove(file)

async def base_message(ctx, title):
    embed = nextcord.Embed(title="On va zouker avec", description=title, color=0x552095)
    embed.set_footer(text="RunBot V3.1")
    skip_interact = Button(label="Skip", custom_id="skip", style=nextcord.ButtonStyle.primary)

    async def skip_song(ctx):
        """Skip the current song"""
        guild_id = ctx.guild.id
        if guild_id in server_sessions:
            session = server_sessions[guild_id]
            voice_client = session.voice_client
            if voice_client.is_playing():
                if len(session.queue) > 1:
                    voice_client.stop()
                else:
                    embed = nextcord.Embed(title="Finito", description="YAR après !", color=0x552095)
                    embed.set_footer(text="RunBot V3.1")
                    await ctx.send(embed=embed)
        
    skip_interact.callback = skip_song

    myview = View(timeout=180)
    myview.add_item(skip_interact)
    await ctx.send(embed=embed, view=myview)

def get_res_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller
     Relative path will always get extracted into root!"""
    base_path = getattr(sys, '_MEIPASS', os.path.dirname(__file__))
    if os.path.isfile(os.path.join(base_path, relative_path)):
        return os.path.join(base_path, relative_path)
    else:
        raise FileNotFoundError(f'Embedded file {os.path.join(base_path, relative_path)} is not found!')

@atexit.register
def cleanup():
    global server_sessions
    for vc in server_sessions.values():
        vc.disconnect()
        vc.cleanup()
    server_sessions = {}
    clean_cache_files()

@bot.event
async def on_ready():
    print(f'Logged in as {bot.user}')

@bot.event
async def on_command_error(ctx, error):
    embed = nextcord.Embed(title=f"Il a tout cassé le fréro {ctx.author}", description=f"{error}", color=0x552095)
    embed.set_footer(text="RunBot V3.1")
    await ctx.send(embed=embed)

# Commandes
@bot.command()
@commands.is_owner()
async def debug(ctx, *, code):
    """Only the bot owner can run this, executes arbitrary code"""
    await ctx.send(eval(code))

async def connect_to_voice_channel(ctx, channel):
    voice_client = await channel.connect()
    if voice_client.is_connected():
        server_sessions[ctx.guild.id] = ServerSession(ctx.guild.id, voice_client)
        return server_sessions[ctx.guild.id]
    else:
        embed = nextcord.Embed(title="Aie ...", description="J\'y arrive quoiqoupas !", color=0x552095)
        embed.set_footer(text="RunBot V3.1")
        await ctx.send(embed=embed)

@bot.command(name='exit')
async def disconnect(ctx):
    """Quitte le channel vocal"""
    guild_id = ctx.guild.id
    if guild_id in server_sessions:
        voice_client = server_sessions[guild_id].voice_client
        await voice_client.disconnect()
        voice_client.cleanup()
        del server_sessions[guild_id]
        embed = nextcord.Embed(title="a+", description=f"{voice_client.channel.name}", color=0x552095)
        embed.set_footer(text="RunBot V3.1")
        await ctx.send(embed=embed)

@bot.command()
async def pause(ctx):
    """Met en pause la musique"""
    guild_id = ctx.guild.id
    if guild_id in server_sessions:
        voice_client = server_sessions[guild_id].voice_client
        if voice_client.is_playing():
            voice_client.pause()
            embed = nextcord.Embed(title="Pause ou quoi", description="J'ai pas raison l'équipe ?", color=0x552095)
            embed.set_footer(text="RunBot V3.1")
            await ctx.send(embed=embed)

@bot.command()
async def resume(ctx):
    """Reprend la musique"""
    guild_id = ctx.guild.id
    if guild_id in server_sessions:
        voice_client = server_sessions[guild_id].voice_client
        if voice_client.is_paused():
            voice_client.resume()
            embed = nextcord.Embed(title="Lezgonge", description="Lezgonge, Lezgonge, Lezgonge !!!", color=0x552095)
            embed.set_footer(text="RunBot V3.1")
            await ctx.send(embed=embed)

@bot.command()
async def skip(ctx):
    """Skip vers la prochaine musique"""
    guild_id = ctx.guild.id
    if guild_id in server_sessions:
        session = server_sessions[guild_id]
        voice_client = session.voice_client
        if voice_client.is_playing():
            if len(session.queue) > 1:
                voice_client.stop()
            else:
                embed = nextcord.Embed(title="Finito", description="YAR après !", color=0x552095)
                embed.set_footer(text="RunBot V3.1")
                await ctx.send(embed=embed)

@bot.command(name='queue')
async def show_queue(ctx):
    """Affiche la playlist"""
    guild_id = ctx.guild.id
    if guild_id in server_sessions:
        await ctx.send(f'{server_sessions[guild_id].display_queue()}')

@bot.command()
async def remove(ctx, i: int):
    """Enlève une musique (1, 2...) `[]`"""
    guild_id = ctx.guild.id
    if guild_id in server_sessions:
        if i == 0:
            await ctx.send('Cannot remove current playing song, please use !skip instead.')
        elif i >= len(server_sessions[guild_id].queue):
            await ctx.send(f'The queue is not that long, there are only {len(server_sessions[guild_id].queue)-1} items in the queue.')
        else:
            removed = server_sessions[guild_id].queue.pop(i)
            removed.audio_source.cleanup()
            await ctx.send(f'Removed {removed} from queue.')

@bot.command()
async def stop(ctx):
    """Vide la playlist et stop la musique"""
    guild_id = ctx.guild.id
    if guild_id in server_sessions:
        voice_client = server_sessions[guild_id].voice_client
        server_sessions[guild_id].queue = []
        if voice_client.is_playing():
            voice_client.stop()
        embed = nextcord.Embed(title="So Longggg", description="C\'est vidé.", color=0x552095)
        embed.set_footer(text="RunBot V3.1")
        await ctx.send(embed=embed)

@bot.command()
async def song(ctx):
    """Montre la musique"""
    guild_id = ctx.guild.id
    if guild_id in server_sessions:
        await base_message(ctx, server_sessions[guild_id].queue[0].title)

@bot.command()
async def play(ctx, *, query: str):
    """Lance une vidéo youtube avec l'URL/mots clés donnée(s) `[]`, ou l'ajoute à la playlist"""
    async with ctx.typing():
        guild_id = ctx.guild.id
        if guild_id not in server_sessions:  # not connected to any VC
            if ctx.author.voice is None:
                embed = nextcord.Embed(title="Bah ...", description="T\'es où mon reuf ?", color=0x552095)
                embed.set_footer(text="RunBot V3.1")
                await ctx.send(embed=embed)
                return
            else:
                session = await connect_to_voice_channel(ctx, ctx.author.voice.channel)
        else:  # is connected to a VC
            session = server_sessions[guild_id]
            if session.voice_client.channel != ctx.author.voice.channel:  # connected to a different VC than the command issuer (but within the same server)
                await session.voice_client.move_to(ctx.author.voice.channel)
        try:
            requests.get(query)
        except (requests.ConnectionError, requests.exceptions.MissingSchema):  # if not a valid URL, do search and play the first video in search result
            query_string = urllib.parse.urlencode({"search_query": query})
            formatUrl = urllib.request.urlopen("https://www.youtube.com/results?" + query_string)
            search_results = re.findall(r"watch\?v=(\S{11})", formatUrl.read().decode())
            url = f'https://www.youtube.com/watch?v={search_results[0]}'
        else:  # is a valid URL, play directly
            url = query
    await session.add_to_queue(ctx, url)  # will download file here
    if not session.voice_client.is_playing() and len(session.queue) <= 1:
        await session.start_playing(ctx)

@bot.command()
async def version(ctx):
    """Montre la version du bot"""
    embed = nextcord.Embed(title="Version", description="C'est le retour de RunBot en 3.0 ou quoi l'équipe ?", color=0x552095)
    embed.set_footer(text="RunBot V3.1")
    await ctx.send(embed=embed)

@bot.command()
async def ping(ctx):
    """Test du ping pong"""
    embed = nextcord.Embed(title="Pong", description="Ping Pong Ping Pong Ping Pong Ping Pong Ping Pong ", color=0x552095)
    embed.set_footer(text="RunBot V3.1")
    await ctx.send(embed=embed)

@bot.command()
async def help_description(ctx):
    """Liste des commandes avec desctiption"""
    embed = nextcord.Embed(title="Liste des commandes avec description", description="Les données entre `[]` sont obligatoires, Les données entre `<>` sont optionnelles.", color=0x552095)
    for command in bot.commands:
        embed.add_field(name=f"+{command.name}", value=command.short_doc, inline=False)
    embed.set_footer(text="RunBot V3.1")
    await ctx.send(embed=embed)

@bot.command()
async def help(ctx):
    """Liste des commandes"""
    embed = nextcord.Embed(title="Liste des commandes", color=0x552095)
    listCommand = ""
    for command in bot.commands:
        listCommand += f"`{command.name}`, "
    listCommand = listCommand[:-2]
    embed.add_field(name="Commandes", value=f"{listCommand}", inline=False)
    embed.set_footer(text="RunBot V3.1")
    await ctx.send(embed=embed)

clean_cache_files()
bot.run(os.environ.get("DISCORD_KEY"))
