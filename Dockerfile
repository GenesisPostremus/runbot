FROM python:3.9

ENV DISCORD_KEY="NTk2MjgyODEwNTYyNTEwODc4.XR3n3A.hfqmuHcM-LbZywMdCUnLHiOvJYA"

WORKDIR /

COPY . ./

RUN pip install --no-cache-dir -r requirements.txt
RUN apt-get update && apt-get install -y ffmpeg
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

EXPOSE 8000

CMD ["python", "app.py"]
